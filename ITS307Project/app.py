import dash
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
from matplotlib import colors
import pandas as pd
import statistics
from plotly import graph_objs as go



df=pd.read_excel('analyseresult.xlsx')
df.dropna(inplace=True)
statistics.mean(df['SEMESTER 1 (%)'])
#average percentage of both cs and it.
ITSEM1MEAN=df.groupby('COURSE', as_index=False)['SEMESTER 1 (%)'].mean()
ITSEM2MEAN=df.groupby('COURSE', as_index=False)['SEMESTER 2 (%)'].mean()
ITSEM3MEAN=df.groupby('COURSE', as_index=False)['SEMESTER 3 (%)'].mean()
ITSEM4MEAN=df.groupby('COURSE', as_index=False)['SEMESTER 4 (%)'].mean()
ITSEM5MEAN=df.groupby('COURSE', as_index=False)['SEMESTER 5 (%)'].mean()

#Percentage of topper
ITCSTOPPERPERSEM1=df.groupby('COURSE', as_index=False)['SEMESTER 1 (%)'].max()
ITCSTOPPERPERSEM2=df.groupby('COURSE', as_index=False)['SEMESTER 2 (%)'].max()
ITCSTOPPERPERSEM3=df.groupby('COURSE', as_index=False)['SEMESTER 3 (%)'].max()
ITCSTOPPERPERSEM4=df.groupby('COURSE', as_index=False)['SEMESTER 4 (%)'].max()
ITCSTOPPERPERSEM5=df.groupby('COURSE', as_index=False)['SEMESTER 5 (%)'].max()

#Minimum Percentage 
ITCSfailuerSEM1=df.groupby('COURSE', as_index=False)['SEMESTER 1 (%)'].min()
ITCSfailuerSEM2=df.groupby('COURSE', as_index=False)['SEMESTER 2 (%)'].min()
ITCSfailuerSEM3=df.groupby('COURSE', as_index=False)['SEMESTER 3 (%)'].min()
ITCSfailuerSEM4=df.groupby('COURSE', as_index=False)['SEMESTER 4 (%)'].min()
ITCSfailuerSEM5=df.groupby('COURSE', as_index=False)['SEMESTER 5 (%)'].min()

#number of studenta that has 60 % and above and below
above60sem1=((df['SEMESTER 1 (%)']>60) &(df['SEMESTER 1 (%)']<100)).sum()
below60sem1=((df['SEMESTER 1 (%)']<60) &(df['SEMESTER 1 (%)']>0)).sum()

above60sem2=((df['SEMESTER 2 (%)']>60) &(df['SEMESTER 2 (%)']<100)).sum()
below60sem2=((df['SEMESTER 2 (%)']<60) &(df['SEMESTER 2 (%)']>0)).sum()

above60sem3=((df['SEMESTER 5 (%)']>60) &(df['SEMESTER 3 (%)']<100)).sum()
below60sem3=((df['SEMESTER 5 (%)']<60) &(df['SEMESTER 3 (%)']>0)).sum()

above60sem4=((df['SEMESTER 4 (%)']>60) &(df['SEMESTER 4 (%)']<100)).sum()
below60sem4=((df['SEMESTER 4 (%)']<60) &(df['SEMESTER 4 (%)']>0)).sum()

above60sem5=((df['SEMESTER 5 (%)']>60) &(df['SEMESTER 5 (%)']<100)).sum()
below60sem5=((df['SEMESTER 5 (%)']<60) &(df['SEMESTER 5 (%)']>0)).sum()

#number of students passed and failed
above50sem1=((df['SEMESTER 1 (%)']>50) &(df['SEMESTER 1 (%)']<100)).sum()
below50sem1=((df['SEMESTER 1 (%)']<50) &(df['SEMESTER 1 (%)']>0)).sum()

above50sem2=((df['SEMESTER 2 (%)']>50) &(df['SEMESTER 2 (%)']<100)).sum()
below50sem2=((df['SEMESTER 2 (%)']<50) &(df['SEMESTER 2 (%)']>0)).sum()

above50sem3=((df['SEMESTER 5 (%)']>50) &(df['SEMESTER 3 (%)']<100)).sum()
below50sem3=((df['SEMESTER 5 (%)']<50) &(df['SEMESTER 3 (%)']>0)).sum()

above50sem4=((df['SEMESTER 4 (%)']>50) &(df['SEMESTER 4 (%)']<100)).sum()
below50sem4=((df['SEMESTER 4 (%)']<50) &(df['SEMESTER 4 (%)']>0)).sum()

above50sem5=((df['SEMESTER 5 (%)']>50) &(df['SEMESTER 5 (%)']<100)).sum()
below50sem5=((df['SEMESTER 5 (%)']<50) &(df['SEMESTER 5 (%)']>0)).sum()

#number of student between 60 to 70 and 70 above
above70sem1=((df['SEMESTER 1 (%)']>70) &(df['SEMESTER 1 (%)']<100)).sum()
between6070sem1=((df['SEMESTER 1 (%)']<70) &(df['SEMESTER 1 (%)']>60)).sum()

above70sem2=((df['SEMESTER 2 (%)']>70) &(df['SEMESTER 2 (%)']<100)).sum()
between6070sem2=((df['SEMESTER 2 (%)']<70) &(df['SEMESTER 2 (%)']>60)).sum()

above70sem3=((df['SEMESTER 3 (%)']>70) &(df['SEMESTER 3 (%)']<100)).sum()
between6070sem3=((df['SEMESTER 3 (%)']<70) &(df['SEMESTER 3 (%)']>60)).sum()

above70sem4=((df['SEMESTER 4 (%)']>70) &(df['SEMESTER 4 (%)']<100)).sum()
between6070sem4=((df['SEMESTER 4 (%)']<70) &(df['SEMESTER 4 (%)']>60)).sum()

above70sem5=((df['SEMESTER 5 (%)']>70) &(df['SEMESTER 5 (%)']<100)).sum()
between6070sem5=((df['SEMESTER 5 (%)']<70) &(df['SEMESTER 5 (%)']>60)).sum()

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP],suppress_callback_exceptions=True,)
server=app.server
# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "lightblue",
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

sidebar = html.Div(
    [
        # html.H5("GCIT Semester Result Analysis", className="display-4"),
        html.P(
            "GCIT Semester Result Analysis", className="lead"),
        html.Hr(),
        dbc.Nav(
            [
                dbc.NavLink("Home", href="https://gcit-result-prediction.herokuapp.com/?fbclid=IwAR0evnABK8wTzCISl6nWK9SVWcF7s78EwHL3rQd2r-AUxkuzXncewuGO5pQ", active="exact"),
                dbc.NavLink("Average Percentage", href="/", active="exact"),
                dbc.NavLink("Maximum Percentage", href="/page-1", active="exact"),
                dbc.NavLink("Minimum Percentage", href="/page-2", active="exact"),
                dbc.NavLink("Number of students with '%' above and below 60", href="/page-3", active="exact"),
                dbc.NavLink("Number of students failed and passed", href="/page-4", active="exact"),
                dbc.NavLink("Number of student with good and verygood rating", href="/page-5", active="exact")
                # dbc.NavLink("No. of students failed and passed in CS", href="/page-6", active="exact"),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

content = html.Div(id="page-content", style=CONTENT_STYLE)

app.layout = html.Div([dcc.Location(id="url"), sidebar, content])


@app.callback(Output("page-content", "children"), [Input("url", "pathname")])
def render_page_content(pathname):
    if pathname == "/":
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Semester', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_years',
                                 multi = False,
                                 clearable = False,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'semester1',
                                 placeholder = 'Select Semester',
                                 options = [{'label': 'semester1','value': 'semester1'},
                                            {'label': 'semester2','value': 'semester2'},
                                            {'label': 'semester3','value': 'semester3'},
                                            {'label': 'semester4','value': 'semester4'},
                                            {'label': 'semester5','value': 'semester5'},], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'top10_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
    elif pathname == "/page-1":
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Semester', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_years',
                                 multi = False,
                                 clearable = False,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'semester1',
                                 placeholder = 'Select Semester',
                                 options = [{'label': 'semester1', 'value': 'semester1'},
                                            {'label': 'semester2','value': 'semester2'},
                                            {'label': 'semester3','value': 'semester3'},
                                            {'label': 'semester4','value': 'semester4'},
                                            {'label': 'semester5','value': 'semester5'},], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'topper_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
    #minimum percentage

    elif pathname == "/page-2":
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Semester', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_years',
                                 multi = False,
                                 clearable = False,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'semester1',
                                 placeholder = 'Select Semester',
                                 options = [{'label': 'semester1', 'value': 'semester1'},
                                            {'label': 'semester2','value': 'semester2'},
                                            {'label': 'semester3','value': 'semester3'},
                                            {'label': 'semester4','value': 'semester4'},
                                            {'label': 'semester5','value': 'semester5'},], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'failure_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
#above and below 60
    elif pathname == "/page-3":
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Semester', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_years',
                                 multi = False,
                                 clearable = False,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'semester1',
                                 placeholder = 'Select Semester',
                                 options = [{'label': 'semester1', 'value': 'semester1'},
                                            {'label': 'semester2','value': 'semester2'},
                                            {'label': 'semester3','value': 'semester3'},
                                            {'label': 'semester4','value': 'semester4'},
                                            {'label': 'semester5','value': 'semester5'},], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'aboveandbelow60_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
    #No of student failed and passed
    elif pathname == "/page-4":
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Semester', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_years',
                                 multi = False,
                                 clearable = False,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'semester1',
                                 placeholder = 'Select Semester',
                                 options = [{'label': 'semester1', 'value': 'semester1'},
                                            {'label': 'semester2','value': 'semester2'},
                                            {'label': 'semester3','value': 'semester3'},
                                            {'label': 'semester4','value': 'semester4'},
                                            {'label': 'semester5','value': 'semester5'},], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'passedfailed50_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])
#no of student between 60 to 70 and 70 above
    elif pathname == "/page-5":
        return html.Div([
            html.Div([
                html.Div([
                    html.P('Select Semester', className = 'fix_label', style = {'color': 'black', 'margin-top': '2px'}),
                    dcc.Dropdown(id = 'select_years',
                                 multi = False,
                                 clearable = False,
                                 disabled = False,
                                 style = {'display': True},
                                 value = 'semester1',
                                 placeholder = 'Select Semester',
                                 options = [{'label': 'semester1', 'value': 'semester1'},
                                            {'label': 'semester2','value': 'semester2'},
                                            {'label': 'semester3','value': 'semester3'},
                                            {'label': 'semester4','value': 'semester4'},
                                            {'label': 'semester5','value': 'semester5'},], className = 'dcc_compon'),

                ], className = "create_container2 six columns", style = {'margin-top': '20px'}),
            ], className = "row flex-display"),
            
            html.Div([
                html.Div([
                    dcc.Graph(id = 'goodverygood_chart',
                              config = {'displayModeBar': 'hover'}),
                ], className = "create_container2 ten columns", style = {'margin-top': '10px'}),
            ], className = "row flex-display"),
        ])

    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ]
    )

@app.callback(
Output('top10_chart', 'figure'), 
[Input('select_years', 'value')]
)
#graph plot and styling
def update_graph(value):
    #semester 1 average percentage of both cs and it
    if value == 'semester1':
        x_semester1 = ITSEM1MEAN.COURSE
        y_semester1 = ITSEM1MEAN['SEMESTER 1 (%)']
        
        return {'data':[go.Bar(
                                x = x_semester1,
                                y = y_semester1,
                                hoverinfo = 'text',
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Average Percentage of CS and IT: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
    #semester 2 average percentage of both cs and it
    if value == 'semester2':
        x_semester2 = ITSEM2MEAN.COURSE
        y_semester2 = ITSEM2MEAN['SEMESTER 2 (%)']  

        return {'data':[go.Bar(
                                x = x_semester2,
                                y = y_semester2,
                                hoverinfo = 'text',
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Average Percentage of CS and IT: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
    #semester 3 average percentage of both cs and it
    if value == 'semester3':
        x_semester3 = ITSEM3MEAN.COURSE
        y_semester3 = ITSEM3MEAN['SEMESTER 3 (%)']  

        return {'data':[go.Bar(
                                x = x_semester3,
                                y = y_semester3,
                                hoverinfo = 'text',
                            
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Average Percentage of CS and IT: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
        
    #semester 4 average percentage of both cs and it
    if value == 'semester4':
        x_semester4 = ITSEM4MEAN.COURSE
        y_semester4 = ITSEM4MEAN['SEMESTER 4 (%)']  

        return {'data':[go.Bar(
                                x = x_semester4,
                                y = y_semester4,
                                hoverinfo = 'text',
                                
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Average Percentage of CS and IT: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
    #semester 5 average percentage of both cs and it
    if value == 'semester5':
        x_semester5 = ITSEM5MEAN.COURSE
        y_semester5 = ITSEM5MEAN['SEMESTER 5 (%)']  

        return {'data':[go.Bar(
                                x = x_semester5,
                                y = y_semester5,
                                hoverinfo = 'text',
                                
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Average Percentage of CS and IT: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }



@app.callback(
Output('topper_chart', 'figure'), 
[Input('select_years', 'value')]
)
# Percentage of each topper in cs and it
def update_graph(value):
    #semester 1 average percentage of both cs and it
    if value == 'semester1':
        x_semester1 = ITCSTOPPERPERSEM1.COURSE
        y_semester1 = ITCSTOPPERPERSEM1['SEMESTER 1 (%)']
        
        return {'data':[go.Bar(
                                x = x_semester1,
                                y = y_semester1,
                                hoverinfo = 'text',
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Percentage of CS and IT topper: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
    #semester 2 average percentage of both cs and it
    if value == 'semester2':
        x_semester2 = ITCSTOPPERPERSEM2.COURSE
        y_semester2 = ITCSTOPPERPERSEM2['SEMESTER 2 (%)']  

        return {'data':[go.Bar(
                                x = x_semester2,
                                y = y_semester2,
                                hoverinfo = 'text',
                                
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Percentage of CS and IT topper: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
    #semester 3 average percentage of both cs and it
    if value == 'semester3':
        x_semester3 = ITCSTOPPERPERSEM3.COURSE
        y_semester3 = ITCSTOPPERPERSEM3['SEMESTER 3 (%)']  

        return {'data':[go.Bar(
                                x = x_semester3,
                                y = y_semester3,
                                hoverinfo = 'text',
                               
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Percentage of CS and IT topper: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
        
    #semester 4 average percentage of both cs and it
    if value == 'semester4':
        x_semester4 = ITCSTOPPERPERSEM4.COURSE
        y_semester4 = ITCSTOPPERPERSEM4['SEMESTER 4 (%)']  

        return {'data':[go.Bar(
                                x = x_semester4,
                                y = y_semester4,
                                hoverinfo = 'text',
                                
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Percentage of CS and IT topper: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
    #semester 5 average percentage of both cs and it
    if value == 'semester5':
        x_semester5 = ITCSTOPPERPERSEM5.COURSE
        y_semester5 = ITCSTOPPERPERSEM5['SEMESTER 5 (%)']  

        return {'data':[go.Bar(
                                x = x_semester5,
                                y = y_semester5,
                                hoverinfo = 'text',
                                
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Percentage of CS and IT topper: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }

#minimum percentage
@app.callback(
Output('failure_chart', 'figure'), 
[Input('select_years', 'value')]
)

def update_graph(value):
    #semester 1 min percentage of both cs and it
    if value == 'semester1':
        x_semester1 = ITCSfailuerSEM1.COURSE
        y_semester1 = ITCSfailuerSEM1['SEMESTER 1 (%)']
        
        return {'data':[go.Bar(
                                x = x_semester1,
                                y = y_semester1,
                                hoverinfo = 'text',
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Percentage of CS and IT topper: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
    #semester 2 min percentage of both cs and it
    if value == 'semester2':
        x_semester2 = ITCSfailuerSEM2.COURSE
        y_semester2 = ITCSfailuerSEM2['SEMESTER 2 (%)']  

        return {'data':[go.Bar(
                                x = x_semester2,
                                y = y_semester2,
                                hoverinfo = 'text',
                               
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Percentage of CS and IT topper: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
    #semester 3 min percentage of both cs and it
    if value == 'semester3':
        x_semester3 = ITCSfailuerSEM3.COURSE
        y_semester3 = ITCSfailuerSEM3['SEMESTER 3 (%)']  

        return {'data':[go.Bar(
                                x = x_semester3,
                                y = y_semester3,
                                hoverinfo = 'text',
                                
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Percentage of CS and IT topper: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
        
    #semester 4 min percentage of both cs and it
    if value == 'semester4':
        x_semester4 = ITCSfailuerSEM4.COURSE
        y_semester4 = ITCSfailuerSEM4['SEMESTER 4 (%)']  

        return {'data':[go.Bar(
                                x = x_semester4,
                                y = y_semester4,
                                hoverinfo = 'text',
                                
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Percentage of CS and IT topper: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }
    #semester 5 min percentage of both cs and it
    if value == 'semester5':
        x_semester5 = ITCSfailuerSEM5.COURSE
        y_semester5 = ITCSfailuerSEM5['SEMESTER 5 (%)']  

        return {'data':[go.Bar(
                                x = x_semester5,
                                y = y_semester5,
                                hoverinfo = 'text',
                                ),
                             ] ,
                'layout': go.Layout(
                            plot_bgcolor = '#F2F2F2',
                            paper_bgcolor = '#F2F2F2',
                            title = {
                                    'text': 'Minimum Percentage of CS and IT: ' + (value),
                                    'y': 0.9,
                                    'x': 0.5,
                                    'xanchor': 'center',
                                    'yanchor': 'top'},
                            titlefont = {
                                     'color': 'black',
                                     'size': 18},
                            hovermode = 'closest',
                            margin = dict(l = 300),
                        
                            xaxis=dict(
                                #type='line',
                                title='<b>COURSE</b>',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),
                            yaxis=dict(
                                title= '<b>Percentage</b>',
                                # autorange = 'reversed',
                                color = 'black',
                                showgrid=True,
                                showline=True,
                                showticklabels = True,
                                linecolor = 'black',
                                linewidth = 1,
                                ticks = 'outside',
                                tickfont = dict(
                                    family = 'Arial',
                                    size = 13,
                                    color = 'black'
                                )
                            ),                         
                        ) 
               }

#percentage above and below 60
@app.callback(
Output('aboveandbelow60_chart', 'figure'), 
[Input('select_years', 'value')]
)

def update_graph(value):
    #semester 1 
    if value == 'semester1':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Above 60', 'Below 60'],
                                   values = [above60sem1,below60sem1],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student below and above 60 %: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
    #semester 2 
    elif value == 'semester2':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Above 60', 'Below 60'],
                                   values = [above60sem2,below60sem2],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student below and above 60 %: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        
    #semester 3 
    if value == 'semester3':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Above 60', 'Below 60'],
                                   values = [above60sem3,below60sem3],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student below and above 60 %: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }

    #semester 4 
    if value == 'semester4':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Above 60', 'Below 60'],
                                   values = [above60sem4,below60sem4],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student below and above 60 %: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
    #semester 5 
    if value == 'semester5':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Above 60', 'Below 60'],
                                   values = [above60sem5,below60sem5],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student below and above 60 %: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }

# no of student failed and passed
@app.callback(
Output('passedfailed50_chart', 'figure'), 
[Input('select_years', 'value')]
)

def update_graph(value):
    #semester 1 
    if value == 'semester1':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Passed', 'Failed'],
                                   values = [above50sem1,below50sem1],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student passed and failed: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
    #semester 2 
    elif value == 'semester2':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Passed', 'Failed'],
                                   values = [above50sem2,below50sem2],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student passed and failed: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
        
    #semester 3 
    if value == 'semester3':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Passed', 'Failed'],
                                   values = [above50sem3,below50sem3],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student passed and failed: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }

    #semester 4 
    if value == 'semester4':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Passed', 'Failed'],
                                   values = [above50sem4,below50sem4],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student passed and failed: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
    #semester 5 
    if value == 'semester5':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['Passed', 'Failed'],
                                   values = [above50sem5,below50sem5],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student passed and failed: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
@app.callback(
Output('goodverygood_chart', 'figure'), 
[Input('select_years', 'value')]
)

def update_graph(value):
    #semester 1 
    if value == 'semester1':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['VeryGood', 'Good'],
                                   values = [above70sem1,between6070sem1],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student with good and verygood rating: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
    elif value == 'semester2':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['VeryGood', 'Good'],
                                   values = [above70sem2,between6070sem2],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student with good and verygood rating: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
    elif value == 'semester3':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['VeryGood', 'Good'],
                                   values = [above70sem3,between6070sem3],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student with good and verygood rating: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
    elif value == 'semester4':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['VeryGood', 'Good'],
                                   values = [above70sem4,between6070sem4],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student with good and verygood rating: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
    elif value == 'semester5':
            color = ['lightblue', 'pink']
        
            return {'data':[go.Pie(labels = ['VeryGood', 'Good'],
                                   values = [above70sem5,between6070sem5],
                                   marker = dict(colors = color),
                                   hoverinfo = 'label+value+percent',
                                   textinfo = 'label+value',
                                   textfont = dict(size = 13),
                                   texttemplate = '%{label}: %{value:} <br>(%{percent})',
                                   textposition = 'auto',
                                   hole = .6,
                                  )] ,
                    'layout': go.Layout(
                        plot_bgcolor = '#F2F2F2',
                        paper_bgcolor = '#F2F2F2',
                        hovermode = 'x',
                        title = {
                            'text': 'Number of student with good and verygood rating: ' + (value),
                            'y': 0.955,
                            'x': 0.5,
                            'xanchor': 'center',
                            'yanchor': 'top'},
                        titlefont = {
                            'color': 'black',
                            'size': 15},
                        legend = {
                            'orientation': 'h',
                            'bgcolor': '#F2F2F2',
                            'xanchor': 'center', 'x': 0.5, 'y': -0.07},
                        font = dict(
                            family = "sans-serif",
                            size = 12,
                            color = 'black')
                    ),
            }
    return app
if __name__ == "__main__":
    app.run_server(debug=True)