#!/usr/bin/env python
# coding: utf-8

# # ITS307 Data Analytics : Assignment 1

# **Gyalpozhing College of Information Technology </br>
# Spring Semester 2022**</br>
# 

# ## Part 1: Drake or Rihanna?

# Billboard Magazine puts out a top 100 list of "singles" every week. Information from this list, as well as that from music sales, radio, and other sources is used to determine a top-100 "singles" of the year list. A single is typically one song, but sometimes can be two songs which are on one "single" record.
# 
# In this homework you will:
# 
# 1. Read data from ``yearinfo.json`` which is scraped from Wikipedia to obtain information about the best singers and groups from each year (distinguishing between the two groups) as determined by the Billboard top 100 charts. You will have to clean this data.First you will learn how to read data from json files and create dataframe.
# 
# 2. Use pandas and matplotlib to represents and explore data

# ### 1.1 Loading data from json file
# 
# Load our JSON file into the ``yearinfo`` variable, just to be sure everything is working.

# In[3]:


#Write your code here, You may need to use python open() function to open the file and load data.
import pandas as pd
import json
import matplotlib.pyplot as plt

# load json file using open()
O=open('/Users/Tshering Gyeltshen/Desktop/Assignments/yearinfo.json')
yearinfo=json.load(O)
yearinfo


# In[4]:


# To check keys in yearinfo json file
yearinfo.keys()


# In[5]:


# To check how many values(song) in year 2021 
len(yearinfo['2021'])


# In[6]:


# print name of columns in year 2021
yearinfo['2021'][0].keys()


# In[7]:


# print names of columns in year 2010
yearinfo['2010'][0].keys()


# In[8]:


# to check values and format of each values in each columns
yearinfo['2021'][0].values()


# ### 1.2 Constructing dataframe from `yearinfo`
# 
# Construct a year-song-singer dataframe from the yearly information
# 
# Let's construct a dataframe **`df`** from the `yearinfo`. The frame should be similar to the frame below. Each row of the frame represents a song, and carries with it the chief properties of year, song, singer, and ranking.
# 
# ![image-3.png](attachment:image-3.png)
# 
# To construct the dataframe, we'll need to iterate over the years and the singles per year. Notice how, above, the dataframe is ordered by ranking and then year. While the exact order is up to you, note that you will have to come up with a scheme to order the information.
# 
# Check that the dataframe has sensible data types. You will also likely find that the year field has become an "object" (Pandas treats strings as generic objects): this is due to the conversion to and back from JSON. Such conversions need special care. Fix any data type issues with `df`. (See Pandas astype function.) We will use this `df` in the next question.

# In[9]:


#write your code here
# Creating list
yearinfo_list = []
for key, values in yearinfo.items():
  for i in values:
    yearinfo_list.append(list(i.values()) + [key])
type(yearinfo_list) 


# In[10]:


#Converting list to DataFrame
df = pd.DataFrame(yearinfo_list)
df.head()


# In[11]:


# naming all columns
df.columns = ['band_singer', 'song', 'songurl', 'ranking', 'titletext', 'url','year']
df.head()


# In[12]:


# checking for null
df.isnull().sum()


# In[13]:


# checking data types
df.dtypes


# In[14]:


# year is in int. changing data type
df.year = df.year.astype('int')


# In[15]:


# checking data types
df.dtypes


# ### 1.3 Who are the highest quality singers?
# 
# Here we show the highest quality singers and plot them on a bar chart.
# 
# #### Find highest quality singers according to how prolific they are
# 
# What do we mean by highest quality? This is of course open to interpretation, but let's define "highest quality" here as the number of times a singer appears in the top 100 over this time period. If a singer appears twice in a year (for different songs), this is counted as two appearances, not one. 
# 
# Make a bar-plot of the most prolific singers. Singers on this chart should have appeared at-least more than 15 times. (HINT: look at the docs for the pandas method `value_counts`.)

# In[16]:


#Write your program and plot bar graph
#Sorting band_singer in list
List = []
for values in df['band_singer']:
  List.append(values)
print(List)


# In[17]:


# Flatten the nested list to single list
newList = [item for elem in List for item in elem]
newList[:2] 
# Showing first two item from single list


# In[18]:


# Converting list to DataFrame
singerdf = pd.DataFrame(newList)
singerdf.columns = ['band_singer']
singerdf.head()


# In[19]:


# replacing old column dataframe with new dataframe 
df['band_singer'] = singerdf['band_singer']
df.head()


# In[20]:


#displaying highest frequency band_singer
bestsinger = df['band_singer'].value_counts()[:20]
bestsinger


# In[21]:


# ploting bar graph
data = pd.DataFrame(bestsinger)
data['band_singer'].plot(kind="bar")
plt.title("best singers based on frequency")
plt.xlabel("band_singer")
plt.ylabel("Frequencies")


# In[22]:


data.columns


# **Question: Who is the most profilic singer?**
# 

# ### 1.4 What if we used a different metric?
# 
# What we would like to capture is this: a singer should to be scored higher if the singer appears higher in the rankings. So we'd say that a singer who appeared once at a higher and once at a lower ranking is a "higher quality" singer than one who appeared twice at a lower ranking.
# 
# To do this, group all of a singers songs together and assign each song a score 101 - ranking. Order the singers by their total score and make a bar chart for the top 20.

# In[23]:


#write your code here
# creating new Metric Dataframe
metricdf = df
metricdf.head()


# In[24]:


# Fetching single ranking value
ranking = metricdf.ranking[0]
type(ranking)
print(type(ranking)) 


# In[25]:


# Creating new score column
def get_ranking(ranking):
  score = 101 - ranking
  return score

metricdf['score'] = metricdf.ranking.map(get_ranking)
metricdf.head()


# In[26]:


# Sorting score column like decending
metricdf.sort_values(['score'],ascending = False, inplace = True)
metricdf.head()


# In[27]:


# Creating new dataframe
total_data = metricdf[['band_singer','score']]
total_data.head()


# In[28]:


# Total score of band_singer
total_score = total_data.groupby('band_singer').sum()
T = total_score.sort_values(by = 'score', ascending = False)
T = T['score'][:20]
# Selecting Top 20 singers


# In[29]:


# Ploting bar plot
top20 = pd.DataFrame(T)
top20['score'].plot(kind = "bar")
plt.ylabel('score')
plt.title("Top 20 singers")


# **Question:Do you notice any major differences when you change the metric? How have the singers at the top shifted places? Why do you think this happens?**

# ## Part 2: Using group properties
# 
# Frame any questions that you want to answer using group properties and interpret your result using appropriate visualizations.
#  

# In[30]:


# what is the best song in the year 2010?


# In[31]:


# Total groups
len(df.groupby('year'))


# In[32]:


df.head()


# In[33]:


songs = [] #creating list
for song in df['titletext']:
    s = song
    songs.append(s.strip('"'))#strip is used to remove "" from titletext
type(songs)
data = pd.DataFrame(songs) #creating Dataframe
data.columns = ['titletext']
df['titletext'] = data['titletext'] #replacing old coloumn with new data
title = df.groupby('year') #group by year
title = title.get_group(2010) 
title = title['titletext'].value_counts(sort = True)[:20] 
new_title = pd.DataFrame(title)
new_title['titletext'].plot(kind = "bar")
plt.ylabel('frequency')
plt.xlabel('songtitle')
plt.title("best 20 songs in year 2010")


# ## SUBMISSION DATE: 2nd April 2022 

# In[ ]:




