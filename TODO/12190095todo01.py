# -*- coding: utf-8 -*-
"""12190095TODO01.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1ydegTvWU5no36S3Beurd5SCLliZvg-tL
"""

import requests
from bs4 import BeautifulSoup

url = 'https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1970'
response = requests.get(url)

soup = BeautifulSoup(response.text)

dictionary = {}

for data in soup.findAll('tr')[1:101]:
  #print(data)

  title = data.findAll('a')[0].string
  print('Title: '+title)

  artist = data.findAll('a')[1].string
  print('Artist: '+artist)

  dictionary[title] = [artist]

print(dictionary)