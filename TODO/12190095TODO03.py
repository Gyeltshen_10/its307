#!/usr/bin/env python
# coding: utf-8

# ## Lab 3: Cleaning and Analysis of Books Dataset
# 

# # Table of Contents 
# <ol start="1">
# <li> Loading and Cleaning with Pandas</li>
# <li> Asking Questions?  </li>
# <li> Parsing and Completing the Dataframe  </li>
# <li> Determining the Best Books  </li>
# <li>Trends in Popularity of Genres </li>
# </ol>

# 
# ## Part 1: Loading and Cleaning with Pandas 
# Read in the `goodreads.csv` file, examine the data, and do any necessary data cleaning. 
# 
# Here is a description of the columns (in order) present in this csv file:
# 
# ```
# rating: the average rating on a 1-5 scale achieved by the book
# review_count: the number of Goodreads users who reviewed this book
# isbn: the ISBN code for the book
# booktype: an internal Goodreads identifier for the book
# author_url: the Goodreads (relative) URL for the author of the book
# year: the year the book was published
# genre_urls: a string with '|' separated relative URLS of Goodreads genre pages
# dir: a directory identifier internal to the scraping code
# rating_count: the number of ratings for this book (this is different from the number of reviews)
# name: the name of the book
# ```

# Load the appropriate libraries

# In[63]:


#write your solution here
import pandas as pd


# ### Cleaning: Reading in the data
# We read in and clean the data from `goodreads.csv`.  Try directly loading the data from file and see what the dataframe look like. What's the problem with naively loading the data as is? You might want to open the CSV file in Excel or your favorite text editor to see how this dataset is formatted.

# In[64]:


#Read the data into a dataframe
#Inser your code
df=pd.read_csv("goodreads.csv")

#Examine the first couple of rows of the dataframe
####### 
#   Insert your code
df.head()
####### 


# Lets read the csv file with custom column descriptions specified in the problem statement.

# In[65]:


#Read the data into a dataframe specifying column names
df=pd.read_csv("goodreads.csv",names=['rating','review_count','isbn','booktype','author_url','year','genre_urls','dir','rating_count','name'])


#Examine the first couple of rows of the dataframe
####### 
#   Insert your code
df.head()
####### 


# 
# ### Cleaning: Examing the dataframe - quick checks
# 
# We should examine the dataframe to get a overall sense of the content.

# In[66]:


#Start by check the column data types
####### 
df.dtypes

####### 


# There are a couple more quick sanity checks to perform on the dataframe. 

# In[67]:


#Come up with a few other important properties of the dataframe to check
####### 
df.isna().sum()
####### 
#we need to check for NaN values, empty strings...these kid of checks are kown as sanity checks.


# **Question:** Was the data read correctly and values represented as we expected?
# 
# Answer : 

# ### Cleaning: Examining the dataframe - a deeper look

# Beyond performing checking some quick general properties of the data frame and looking at the first $n$ rows, we can dig a bit deeper into the values being stored. If you haven't already, check to see if there are any missing values in the data frame.

# In[68]:


#Get a sense of how many missing values there are in the dataframe.
####### 
df.isna().sum()
####### 


# In[69]:


#Try to locate where the missing values occur
####### 
df[df.rating.isna()]
####### 


# In[70]:


df.shape


# How does `pandas` or `numpy` handle missing values when we try to compute with data sets that include them?
# 
# dropna() which drops all the NaN, None values.
# 

# ### Cleaning: Dealing with Missing Values
# How should we interpret 'missing' or 'invalid' values in the data (hint: look at where these values occur)? One approach is to simply exclude them from the dataframe. Is this appropriate for all 'missing' or 'invalid' values? How would you drop these values from the dataframe (hint: is it possible to eliminate just a single entry in your dataframe? Should you eliminate an entire row? Or column?)?

# In[71]:



#Treat the missing or invalid values in your dataframe
####### 
df.dropna(subset=['rating','year'],inplace=True)
df.shape
####### 


# Ok so we have done some cleaning. Is it enough? 

# In[72]:


#Check the column data types again
####### 
df.dtypes
####### 


# Notice the float has not yet changed.
# Ok so lets fix those types. If the type conversion fails, we now know we have further problems.

# In[73]:


#Convert rating_count, review_count and year to int 
#######
df['rating_count']=df['rating_count'].astype('int')
df['review_count']=df['review_count'].astype('int')
df['year']=df['year'].astype('int')
df.dtypes
#######


# Final check

# Some of the other colums that should be strings have NaN. 

# In[74]:


#Insert Your code here
df.isna().sum()


# ##  Part 2: Asking Questions
# Think of few questions we want to ask and then examine the data and decide if the dataframe contains what you need to address these questions. 
# 
# **Example:** Which are the highest rated books? To determine this, you'll only need the data in two columns: `name` and `rating`. The task will be to sort these two columns by the value in `rating`.
# 

# 

# In[75]:


Book = df.groupby('year',sort=False)


# In[76]:


YEAR_2008=Book.get_group(2008)

bestbookof2008=YEAR_2008[YEAR_2008['rating_count']==YEAR_2008.rating_count.max()]
print(bestbookof2008)


# In[82]:


authordf=df.groupby('author',sort=False)
print(authordf.size())
print(authordf.first())
print(authordf.last())
print(authordf.groups)


# In[ ]:


authorfirst=authordf.get_group('Suzanne_Collins')
print(authorfirst)


# In[ ]:


df['year'].mean()


# In[ ]:





# In[ ]:


# df.agg({"rating":'max',"review_count":'mean'})


# In[ ]:





# ##  Part 3: Parsing and Completing the Data Frame 
# 
# We will need author and genre to proceed! Parse the `author` column from the author_url and `genres` column from the genre_urls. Keep the `genres` column as a string separated by '|'.
# 
# Hint: Use panda's `map` to assign new columns to the dataframe.  
# 
# 
# ---

# Examine an example `author_url` and reason about which sequence of string operations must be performed in order to isolate the author's name.

# In[79]:


#Get the first author_url
author=df['author_url'][0]
author


# In[80]:


#Test out some string operations to isolate the author name
####### 
name=author.split(".")[-1]
name
#######


# In[81]:


#Write a function that accepts an author url and returns the author's name based on your experimentation above
    ####### 
def get_author(url):
    name=url.split(".")[-1]
    return name
    

#Apply the get_author function to the 'author_url' column using '.map' 
#and add a new column 'author' to store the names
df['author']=df.author_url.map(get_author)
df.head()


# Now parse out the genres from `genre_url`.  Like with the authors, we'll assign a new `genres` column to the dataframe.
# 
# This is a little more complicated because there be more than one genre.

# In[ ]:


#Get first genre_url
gdf=df.dropna(subset=['genre_urls'])
gdf.shape


# In[ ]:


firstgenre=gdf['genre_urls'][0]
firstgenre


# In[ ]:


genres=firstgenre.split("/genres/")[1:]
firstgenre


# In[ ]:


#Test out some string operations to isolate the genres
####### 
genres=" ".join(genres)
genres
#######


# In[ ]:


#Write a function that accepts a genre url and returns the genre name based on your experimentation above
    ####### 
def get_genre(url):
    genres=url.split("/genres/")[1:]
    genres="".join(genres)
    return genres

gdf['genres']=gdf.genre_urls.map(get_genre)
gdf
    ####### 
gdf.head()


# In[ ]:


# EDA (Exploratory data analysis)-graphical and non-graphical
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
x=df.review_count
plt.hist(x,bins=13,log=True)
plt.xlabel("review_count")
plt.ylabel("frequency")
plt.show()


# In[ ]:


# Bar Plot


# ---

# 
# ## Part 4:  Determining the Best Books 
# 
# This is an example of an analysis of the "grouped property" type.
# 
# Think of some reasonable definitions of what it could mean to be a "best book."
#  
# 
# ---

# For example, we can determine the "best book" by year! Determine the best books in each year from 2000

# In[ ]:


#Deterime best book each year based on rating. 
book=df.groupby('author')
t=book.get_group('Suzanne_Collins')

high = t[t['review_count']==t.review_count.max()]
print(high.review_count,high.name)


# In[ ]:


highestRC=df.groupby('author')
highestRC.size()

for key, value in highestRC:
    highestRC =(value[value['review_count']==value.review_count.max()])
    print(highestRC.name, highestRC.author,highestRC.review_count)
    break


# In[ ]:





# Try this for few other definitions of the "best book" using `.groupby`.

# ## Part 5:  Trends in Popularity of Genres 
# 
# This is an example of an analysis of the "grouped property" type.
# 
# There are a lot of questions you could ask about genres.
# * Which genre is currently the most popular?
# * Better, based on our data, what draw conclusions can you draw about the time evolution of the popularity of each genre?
# 
# 
# ---

# First we need to find the distinct genres in the dataframe. 
# 
# To do this, notice that each string is a pipe (|) separated list of genres. For each string, we ask if the genre is in that pipe separated list.  If it is, we return True, else False
# 
# **Hint: remember that python sets have unique (non-repeating) items.**

# What happens if we add a column to the dataframe for each genre? 
# 
# Is this way of representing genre efficient? Allows for easy computation and visualization?
# 
# Are there other ways to represent genre information in the dataframe that allow for each visualization?

# In[78]:


#Explore different ways to visualize information about the genres in the dataframe
####### 
set_genre=set()
for genre in gdf.genres:
    set_genre.update(genre.split("|"))
print(len(set_genre))
####### 


# In[ ]:


#plot the data here


# In[ ]:




