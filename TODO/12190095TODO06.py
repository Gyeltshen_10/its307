#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# In[2]:


#Loading the data
df=pd.read_csv('loan_train.csv')
df.head()


# In[3]:


df.dtypes


# In[4]:


df.shape


# In[5]:


df.isna().sum()


# In[6]:


df.dropna(inplace=True)


# In[7]:


df.shape


# # 1. OUTLIER DETECTION

# In[8]:


#Boxplot to check outliers in income of the applicant
sns.boxplot(x=df['ApplicantIncome'])
plt.show()


# In[9]:


# calculating IQR and upper limit and lower limit to find outliers
Q1 = df.ApplicantIncome.quantile(0.25)
Q3 = df.ApplicantIncome.quantile(0.75)
IQR = Q3 - Q1
upperlimit = Q3 + (IQR * 1.5)
lowerlimit = Q1 - (IQR * 1.5)


# In[10]:


#Checking the outliers
df[(df.ApplicantIncome < lowerlimit)  | (df.ApplicantIncome > upperlimit)]


# In[11]:


# Droping the rows containing height beyond lower and upper limit.
df1 = df[(df.ApplicantIncome > lowerlimit) & (df.ApplicantIncome < upperlimit)]
df1.shape


# In[12]:


sns.boxplot(x=df1['ApplicantIncome'])
plt.show()


# # 2.TRANSFORMATION

# In[13]:


#Distribution before transformation
plt.hist(df1.ApplicantIncome,bins=20, rwidth=0.8)
plt.xlabel("Income of the Applicant")
plt.show()


# In[14]:


#Log Transformation
x = np.log(df1['ApplicantIncome'])
plt.hist(x,bins=20,rwidth=0.8)
plt.xlabel("Income of the Applicant")
plt.show()


# In[15]:


#Cuberoot Transformation
x = np.cbrt(df1.ApplicantIncome)
plt.hist(x,bins=20,rwidth=0.8)
plt.xlabel("Income of the Applicant")
plt.show()


# # 3. Feature Scaling

# In[16]:


#MinMax Scaler
df1.dtypes


# In[17]:


numeric_columns = ['ApplicantIncome','LoanAmount','Loan_Amount_Term','Credit_History']
numericdf = df1[numeric_columns]
#Dataframe before scaling
numericdf


# In[18]:


numericdf.isna().sum()


# In[19]:


from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler(feature_range=(0, 1))
numeric = scaler.fit_transform(numericdf)


# In[20]:


#dataframe after scaling
transformdf = pd.DataFrame(data=numeric,columns=numeric_columns)
transformdf.head()


# In[21]:


sns.set(rc={'figure.figsize':(8,6)})

sns.scatterplot(data=df1,x='ApplicantIncome',y='LoanAmount')
sns.scatterplot(data = transformdf,x='ApplicantIncome',y='LoanAmount')
plt.show()


# In[22]:


#ROBUST SCALER
from sklearn.preprocessing import RobustScaler
scaler = RobustScaler()
numeric = scaler.fit_transform(numericdf)


# In[23]:


robustdf = pd.DataFrame(numeric,columns=numeric_columns)
robustdf.head()


# # 4. Encoding Categorical variables

# In[24]:


#One-Hot Encoding using Pandas get_dummies()
df1.head()


# In[25]:


df1.Education.unique()


# In[26]:


categorical_columns=["Gender",'Married']
cdf = df1[categorical_columns]
cdf.head()


# In[27]:


#creating dummy variables for nominal categorical variable using get_dummies()
categorical_columns=["Gender",'Married']
cdf = df1[categorical_columns]
dummies_df = pd.get_dummies(cdf)
dummies_df.head()


# In[28]:


#LabelEncoder
from sklearn.preprocessing import LabelEncoder
le =  LabelEncoder()
df1['Loan_Status'] = le.fit_transform(df1.Loan_Status)
df1.head()


# # 5. Feature Selection

# In[38]:


corr = df1.corr()
sns.set(rc={'figure.figsize':(15,10)})
sns.heatmap(data= corr,annot=True)
plt.show()


# In[39]:


corr.columns


# In[40]:


def correlation_matrix(dataset,threshold):
    col_corr = set()
    corr_matrix = dataset.corr()
    for i in range(len(corr_matrix.columns)):
        for j in range(i):
            if abs(corr_matrix.iloc[i,j]) > threshold:
                colname = corr_matrix.columns[i]
                col_corr.add(colname)
    return col_corr


# In[41]:


corr_features = correlation_matrix(df1, 0.7)
corr_features


# In[52]:


newdf = transformdf.drop(corr_features,axis = 1)
newdf.head()


# # Training the model

# In[53]:


y=df1.Loan_Status
x=newdf


# In[54]:


from sklearn.model_selection import train_test_split
xtrain, xtest, ytrain, ytest = train_test_split(x,y,test_size=0.2)


# In[55]:


from sklearn.linear_model import LinearRegression
model = LinearRegression()
model.fit(xtrain, ytrain)
model.score(xtrain, ytrain)


# In[56]:


model.score(xtest, ytest)


# In[ ]:




