#!/usr/bin/env python
# coding: utf-8

# # ITS307 Data Analytics                                                   : Spring Semester 2022
# # Practical 5
# # Supervised Learning: Linear Regression
# 
# ![image-3.png](attachment:image-3.png)

# # Table of Contents 
# <ol start="0">
# <li> Learning Objectives </li>
# <li> Importing Libraries </li>
# <li> Loading and Cleaning with Pandas</li>
# <li> EDA  </li>
# <li> Training Model</li>
# <li> Predicting</li>
# </ol>

# ## Learning Objectives
# 
# This dataset contains information collected by the U.S Census Service concerning housing in the area of Boston Mass. It was obtained from the StatLib archive (http://lib.stat.cmu.edu/datasets/boston), and has been used extensively throughout the literature to benchmark algorithms. However, these comparisons were primarily done outside of Delve and are thus somewhat suspect. The dataset is small in size with only 506 cases.
# 
# The data was originally published by Harrison, D. and Rubinfeld, D.L. `Hedonic prices and the demand for clean air', J. Environ. Economics & Management, vol.5, 81-102, 1978.
# 
# ## Variables
# There are 14 attributes in each case of the dataset. They are:
# 1. CRIM - per capita crime rate by town
# 2. ZN - proportion of residential land zoned for lots over 25,000 sq.ft.
# 3. INDUS - proportion of non-retail business acres per town.
# 4. CHAS - Charles River dummy variable (1 if tract bounds river; 0 otherwise)
# 5. NOX - nitric oxides concentration (parts per 10 million)
# 6. RM - average number of rooms per dwelling
# 7. AGE - proportion of owner-occupied units built prior to 1940
# 8. DIS - weighted distances to five Boston employment centres
# 9. RAD - index of accessibility to radial highways
# 10. TAX - full-value property-tax rate per $10,000
# 
# 11. PTRATIO - pupil-teacher ratio by town
# 12. B - 1000(Bk - 0.63)^2 where Bk is the proportion of blacks by town
# 13. LSTAT - % lower status of the population
# 14. MEDV - Median value of owner-occupied homes in $1000's (**TARGET**)
# 
# By the end of the lab, you should be able to :
# - Load and systematically address missing values, ancoded as NaN values in our data set, for example, by removing observations associated with these values.
# 
# - Parse columns in the dataframe to create new dataframe columns.
# 
# - Create and interpret visualizations to explore the data set and relationships between variables
# - Create a simple Linear Model to predict Housing prize given features values

# ## Importing Libraries

# In[19]:


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_boston


# # Part 1: Simple Linear Regression

# ## Loading and Cleaning data with pandas

# In[20]:


data = load_boston()
for keys in data:
    print(keys)


# In[3]:


print(data.DESCR)


# In[21]:


data


# In[22]:


df = pd.DataFrame(data.data,columns = data.feature_names)
df.head()


# In[23]:


df.shape


# In[24]:


df['MEDV'] = data.target
df.head()


# ## EDA
# 
# Choose any features to explore relationship with target variables.

# In[25]:


import seaborn as sns
sns.set(rc={'figure.figsize':(15.7,8.27)})
sns.scatterplot(data = df,x = 'RM',y = 'MEDV')
plt.xlabel("Number of rooms")
plt.ylabel("Median Value in $1000's")
plt.show()


# ## Training Model

# In[26]:


#create instance of LinearRegression
model = LinearRegression()
model


# In[27]:


model.fit(np.array(df.RM).reshape(-1,1),df.MEDV)
model


# In[11]:


model.coef_


# In[12]:


model.intercept_


# ## Prediction

# In[28]:


x = np.array([6.25]).reshape(-1,1)
model.predict(x)


# ## Check MSE

# In[31]:


x = np.array(df.RM).reshape(-1,1)


# In[32]:


from sklearn.metrics import mean_squared_error
y_pred = model.predict(x)


# In[33]:


MSE = mean_squared_error(df.MEDV,y_pred)


# In[34]:


MSE


# In[35]:


sns.set(rc={'figure.figsize':(15.7,8.27)})
sns.scatterplot(data = df,x = 'RM',y = 'MEDV')
plt.plot(x, y_pred,color='red')
plt.xlabel("Number of rooms")
plt.ylabel("Median Value in $1000's")
plt.show()


# # Part 2: Multiple Linear Regression(TODO 5)

# ## EDA

# In[36]:


sns.set(rc={'figure.figsize':(15.7,8.27)})
sns.scatterplot(data = df,x = 'CRIM',y = 'MEDV')
plt.xlabel("Crime Rate")
plt.ylabel("Median Value in $1000's")
plt.show()


# In[37]:


sns.set(rc={'figure.figsize':(15.7,8.27)})
sns.scatterplot(data = df,x = 'NOX',y = 'MEDV')
plt.xlabel("nitric oxides concentration")
plt.ylabel("Median Value in $1000's")
plt.show()


# In[38]:


sns.set(rc={'figure.figsize':(15.7,8.27)})
sns.scatterplot(data = df,x = 'LSTAT',y = 'MEDV')
plt.xlabel("lower status of the population")
plt.ylabel("Median Value in $1000's")
plt.show()


# ## Training Multiple Regression Model

# In[39]:


model.fit(np.array(df[['CRIM','NOX','LSTAT']]).reshape(-1,3),df.MEDV)
model


# In[40]:


LinearRegression()


# In[41]:


model.coef_


# In[42]:


model.intercept_


# In[43]:


df.head()


# ## Predicting Value

# In[44]:


x = np.array([0.01156,0.456,6.54]).reshape(-1,3)
model.predict(x)


# In[45]:


#checking the model by mapping the output value with the dataset value
x = np.array([0.00632,0.538,4.98]).reshape(-1,3)
model.predict(x)

